#![deny(warnings)]
extern crate warp;

use warp::Filter;

fn main() {
    let routes = warp::any().map(|| "Hello, Warp");

    println!("Starting server on port 8000...");
    warp::serve(routes).run(([127, 0, 0, 1], 8000));
}
